package nl.yeswayit.ordina.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("WordFrequency")
public class JAXRSConfiguration extends Application {

}