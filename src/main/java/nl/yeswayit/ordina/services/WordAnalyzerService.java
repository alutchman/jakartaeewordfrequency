package nl.yeswayit.ordina.services;

import nl.yeswayit.ordina.json.*;
import nl.yeswayit.ordina.utils.WordFrequency;
import nl.yeswayit.ordina.utils.WordFrequencyAnalyzer;
import nl.yeswayit.ordina.utils.WordFrequencyInfo;

import javax.ejb.Stateless;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Stateless
public class WordAnalyzerService implements WordFrequencyAnalyzer {

    public ResponseWordFreq calculateHighestFrequency(RequestHighestFreq request) {
        ResponseWordFreq response = new ResponseWordFreq();
        response.setData(request.getData());
        if (request.getData() == null || request.getData().trim().length() ==0 ) {
            response.setFrequency(0);
            return response;
        }
        List<WordFrequency> wordlist = orginaizeText(request.getData());

        WordFrequency freqInfo = wordlist.get(0);
        response.setData(request.getData());
        response.setWord(freqInfo.getWord());
        response.setFrequency(freqInfo.getFrequency());
        return response;
    }

    public ResponseWordFreq calculateFrequencyForWord(RequestWordFreq request) {
        WordFrequencyInfo freqInfo = new WordFrequencyInfo(request.getWord());
        Arrays.asList(request.getData().split("\\s")).forEach(input -> {
            if (input.equalsIgnoreCase(request.getWord())) {
                freqInfo.incFrequency();
            }
        });
        ResponseWordFreq response = new ResponseWordFreq();
        response.setData(request.getData());
        response.setWord(freqInfo.getWord());
        response.setFrequency(freqInfo.getFrequency());
        return response;
    }


    public ResponseMultipleWordsFreq calculateMostFrequentNWords(RequestWordsFreq request) {
        List<WordFrequency> wordlist = orginaizeText(request.getData());
        int maxNum = wordlist.size();
        List<WordFrequency> result;
        if (request.getNumber() < maxNum) {
            result =  wordlist.subList(0, request.getNumber());
        } else {
            result = wordlist;
        }

        List<ResponseWordOfManyFreq> values = new ArrayList<>();
        Consumer<WordFrequency> transformForResonse = item -> {
            ResponseWordOfManyFreq transformed = new ResponseWordOfManyFreq();
            transformed.setWord(item.getWord());
            transformed.setFrequency(item.getFrequency());
            values.add(transformed);
        };
        result.forEach(transformForResonse);

        ResponseMultipleWordsFreq response = new ResponseMultipleWordsFreq();
        response.setData(request.getData());
        response.setNumber(request.getNumber());
        response.setValues(values);
        return response;
    }


    private List<WordFrequency> orginaizeText(String text) {
        List<String> wordlist = Arrays.asList(text.split("\\s"));
        final Map<String, WordFrequencyInfo> mapped = new HashMap<String, WordFrequencyInfo>();

        Consumer<String> organizer = input -> {
            String toMap = input.toLowerCase();
            if (mapped.containsKey(toMap)) {
                WordFrequencyInfo existingItem = mapped.get(toMap);
                existingItem.incFrequency();
            } else {
                WordFrequencyInfo newItem = new WordFrequencyInfo(toMap);
                newItem.incFrequency();
                mapped.put(toMap, newItem);
            }
        };
        wordlist.forEach(organizer);
        List<WordFrequency> preList = mapped.values().stream().collect(Collectors.toList());

        preList.sort((u, v) -> {
            if (u.getFrequency() == v.getFrequency()) {
                return String.CASE_INSENSITIVE_ORDER.compare(u.getWord(), v.getWord());
            }
            return u.getFrequency() > v.getFrequency() ? -1 : 1;
        });
        return preList;
    }


}
