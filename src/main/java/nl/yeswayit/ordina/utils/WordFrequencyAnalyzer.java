package nl.yeswayit.ordina.utils;

import nl.yeswayit.ordina.json.*;

public interface WordFrequencyAnalyzer {
    ResponseWordFreq calculateHighestFrequency(RequestHighestFreq request);
    ResponseWordFreq calculateFrequencyForWord(RequestWordFreq request);
    ResponseMultipleWordsFreq calculateMostFrequentNWords(RequestWordsFreq request);
}
