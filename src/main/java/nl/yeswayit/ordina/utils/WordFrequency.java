package nl.yeswayit.ordina.utils;

public interface WordFrequency {
    String getWord();
    int getFrequency();
}