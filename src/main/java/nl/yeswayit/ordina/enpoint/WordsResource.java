package nl.yeswayit.ordina.enpoint;

import nl.yeswayit.ordina.json.RequestHighestFreq;
import nl.yeswayit.ordina.json.ResponseWordFreq;
import nl.yeswayit.ordina.utils.WordFrequencyAnalyzer;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("highest")
public class WordsResource {
    @Inject
    private WordFrequencyAnalyzer service;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHighestFrequency(RequestHighestFreq request) {
        ResponseWordFreq resp = service.calculateHighestFrequency(request);
        return Response.ok().type(MediaType.APPLICATION_JSON_TYPE).entity("resp").build();
    }
}
