package nl.yeswayit.ordina.json;

import java.util.ArrayList;
import java.util.List;

public class ResponseMultipleWordsFreq {
    private String data;
    private int number;
    private List<ResponseWordOfManyFreq> values = new ArrayList<>();

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public List<ResponseWordOfManyFreq> getValues() {
        if (values  == null) {
            values = new ArrayList<>();
        }
        return values;
    }

    public void setValues(List<ResponseWordOfManyFreq> values) {
        this.values = values;
    }
}
