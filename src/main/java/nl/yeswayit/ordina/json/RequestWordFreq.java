package nl.yeswayit.ordina.json;

public class RequestWordFreq {
    private String data;
    private String word;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
