package nl.yeswayit.ordina.json;

public class ResponseWordOfManyFreq {
    private int frequency;
    private String word;

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
