package nl.yeswayit.ordina;

import nl.yeswayit.ordina.enpoint.WordsResource;
import nl.yeswayit.ordina.json.RequestHighestFreq;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.microshed.testing.jaxrs.RESTClient;
import org.microshed.testing.jupiter.MicroShedTest;
import org.microshed.testing.testcontainers.ApplicationContainer;
import org.testcontainers.junit.jupiter.Container;

import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Disabled
@MicroShedTest
public class HighFreqResourceIT {
    @Container
    public static ApplicationContainer app = new ApplicationContainer()
            .withAppContextRoot("/WordFrequency")
            .withReadinessPath("/WordFrequency/highest");

    @RESTClient
    public static WordsResource testEndpoint;

    @Test
    public void message() {
        RequestHighestFreq request = new RequestHighestFreq();
        request.setData("Nog een zin en nog meer");
        Response result = testEndpoint.getHighestFrequency(request);
        assertNotNull(result);
    }
}
