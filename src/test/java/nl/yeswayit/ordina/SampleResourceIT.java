package nl.yeswayit.ordina;

import nl.yeswayit.ordina.enpoint.SampleResource;
import org.junit.jupiter.api.Test;
import org.microshed.testing.jaxrs.RESTClient;
import org.microshed.testing.jupiter.MicroShedTest;
import org.microshed.testing.testcontainers.ApplicationContainer;
import org.testcontainers.junit.jupiter.Container;

import javax.ws.rs.core.Response;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@MicroShedTest
public class SampleResourceIT {
    @Container
    public static ApplicationContainer app = new ApplicationContainer()
            .withAppContextRoot("/WordFrequency")
            .withReadinessPath("/WordFrequency/sample");

    @RESTClient
    public static SampleResource testEndpoint;

    @Test
    public void message() {
        Response result = testEndpoint.message();
        assertNotNull(result);
    }
}
