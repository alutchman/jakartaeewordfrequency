# Project review-microshed-testing

Steps to run this project:

1. Start your Docker daemon
2. Execute `./run.sh` (Linux/MacOs)
3. Wait until Open Liberty is up- and running (e.g. use `docker logs -f CONTAINER_ID`)
4. Visit http://localhost:9080/WordFrequency/sample